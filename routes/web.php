<?php

Route::get('/', 'MainController@index')->name('main');

Route::get('/categories', 'ProductController@categories')->name('categories');


//Корзина
Route::group(['prefix' => 'cart'] , function () {
    Route::post('/add/{id}', 'CartController@cartAdd')->name('cart-add');

    Route::group(['middleware' => 'cartIsNotEmpty'],
        function () {
            Route::get('/', 'CartController@cart')->name('cart');
            Route::post('/confirm', 'CartController@cartСonfirm')->name('cart-confirm');
            Route::post('/remove/{id}', 'CartController@cartRemove')->name('cart-remove');
        });
});

//Вывод категорий, товаров опред категор, карточка продукта в категории
Route::get('/categories/{category}', 'ProductController@category')->name('category');
Route::get('/categories/{category}/{product?}', 'ProductController@showproduct')->name('product');

//Работа с профилем
Route::get('/profile', 'ProfileController@index')->name('profile.profile');
Route::get('/profile/edit','ProfileController@getEdit')->name('profile.edit');
Route::post('/profile/edit','ProfileController@postEdit')->name('profile.edit');

//Админка
Route::group(['middleware' => ['auth','isAdmin'], 'namespace' => 'Admin'], function () {
    Route::get('/adminPanel','AdminController@index')->name('adminPanel');
    Route::resource('/adminPanel/Category','CategoryController');
    Route::resource('/adminPanel/Payments','PaymentsController');
    Route::resource('/adminPanel/Manufacturers','ManufacturersController');
    Route::resource('/adminPanel/Deliveries','DeliveriesController');
    Route::resource('/adminPanel/Products','ProductsController');
    Route::resource('/adminPanel/Orders','OrdersController');
    Route::resource('/adminPanel/Users','UsersController');
});


//Аунтефикация
Auth::routes();
Route::get('signinup','MainController@signinup')->name('sign');
//Route::get('/home', 'HomeController@index')->name('home');
