@extends('layouts.master')
@section('content')
        <auth-forms login-route="{{ route('login') }}" register-route="{{ route('register') }}"></auth-forms>
@endsection
