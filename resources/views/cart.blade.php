@extends('layouts.master')
@section('title','Корзина')
@section('content')
    <section>
        @if(!is_null($order))
                <article class="flex justify-center my-6">
                    <div class="flex flex-col w-full p-8 text-gray-800 bg-white shadow-lg pin-r pin-y md:w-4/5 lg:w-4/5">
                        <div class="flex-1">
                            <table class="w-full text-sm lg:text-base" cellspacing="0">
                                <thead>
                                <tr class="h-12 uppercase">
                                    <th class="hidden md:table-cell"></th>
                                    <th class="text-left">Товар</th>
                                    <th class="lg:text-right text-left pl-5 lg:pl-0">Количество</th>
                                    <th class="hidden text-right md:table-cell">Цена за единицу</th>
                                    <th class="text-right">Итоговая цена</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($order->products as $product)
                                <tr>
                                    <td class="hidden pb-4 md:table-cell">
                                        <a href="#">
                                            <img src="{{ Storage::url($product->preview) }}" class="w-10 rounded" alt="{{ $product->name }}">
                                        </a>
                                    </td>
                                    <td>
                                        <p class="mb-2 md:ml-4">{{ $product->name }}</p>
                                        <form action="{{ route('cart-add', $product) }}" method="post">
                                            @csrf
                                            <button type="submit" class="text-gray-700 md:ml-4">
                                                <small>Добавить товар</small>
                                            </button>
                                        </form>
                                        <form action="{{ route('cart-remove', $product) }}" method="post">
                                            @csrf
                                            <button type="submit" class="text-gray-700 md:ml-4">
                                                <small>Удалить товар</small>
                                            </button>
                                        </form>
                                    </td>
                                    <td class="justify-center md:justify-end md:flex mt-6">
                                        <div class="w-20 h-10 text-center">
                                            <span>{{ $product->pivot->count }}</span>
                                        </div>
                                    </td>
                                    <td class="hidden text-right md:table-cell">
                                        <span class="text-sm lg:text-base font-medium">{{ $product->price }} руб.</span>
                                    </td>
                                    <td class="text-right">
                                        <span class="text-sm lg:text-base font-medium">{{ $product->getPriceForCount() }} руб.</span>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <hr class="pb-6 mt-6">
                            <span>Общая стоимость – {{ $order->amount() }} рублей</span>
                            <div class="my-4 mt-6 lg:flex">
                                <div class="lg:px-2 lg:w-1/2">
                                    <div class="p-4">
                                        <form action="{{ route('cart-confirm') }}" method="post">
                                            @csrf
                                            <div class="flex justify-between pt-4 border-b">
                                                <div class="lg:px-4 lg:py-2 m-2 text-lg lg:text-xl font-bold text-center text-gray-800">
                                                    <label for="">Способ доставки:</label>
                                                    @foreach($shipping as $delivery)
                                                        <span>{{ $delivery->name }}</span><input type="radio" name="deliver" value="{{ $delivery->id }}">
                                                    @endforeach
                                                </div>
                                                <div class="lg:px-4 lg:py-2 m-2 lg:text-lg font-bold text-center text-gray-900">
                                                    <label for="">Тип оплаты:</label>
                                                    @foreach($payments as $payment)
                                                        <span>{{ $payment->type }}</span><input type="radio" name="pay" value="{{ $payment->id }}">
                                                    @endforeach
                                                </div>
                                            </div>
                                            <textarea name="comment" type="text" placeholder="Введите комментарий к заказу" rows="8" cols="80" class="autoexpand tracking-wide py-2 px-4 mb-3 leading-relaxed appearance-none block w-full bg-gray-200 border border-gray-200 rounded focus:outline-none focus:bg-white focus:border-gray-500"></textarea>
                                            <button class="flex justify-center w-full px-10 py-3 mt-6 font-medium text-white uppercase bg-gray-800 rounded-full shadow item-left hover:bg-gray-700 focus:shadow-outline focus:outline-none">
                                                <svg aria-hidden="true" data-prefix="far" data-icon="credit-card" class="w-8" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M527.9 32H48.1C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48.1 48h479.8c26.6 0 48.1-21.5 48.1-48V80c0-26.5-21.5-48-48.1-48zM54.1 80h467.8c3.3 0 6 2.7 6 6v42H48.1V86c0-3.3 2.7-6 6-6zm467.8 352H54.1c-3.3 0-6-2.7-6-6V256h479.8v170c0 3.3-2.7 6-6 6zM192 332v40c0 6.6-5.4 12-12 12h-72c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h72c6.6 0 12 5.4 12 12zm192 0v40c0 6.6-5.4 12-12 12H236c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h136c6.6 0 12 5.4 12 12z"/></svg>
                                                <span class="ml-2 mt-5px">Оформить заказ</span>
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
        @else
            <article class="text-center mt-5">
                <span class="py-3 px-2 my-2 bg-yellow-300 text-yellow-800 rounded border border-yellow-600">Корзина пустая, выберите товары из каталога!</span>
            </article>
        @endif
    </section>
@endsection
