@extends('layouts.master')
@section('title',$category->name)
@section('content')
    <div class="text-center">
        <h1>{{ $category->name.' '.$count->count }}</h1>
    </div>
    <section class="grid grid-cols-3">
        @foreach($category->products as $product)
            <article class="max-w-xs bg-white shadow-lg rounded-lg overflow-hidden mt-20 m-auto">
                <div class="px-4 py-2">
                    <h3 class="text-gray-900 font-bold text-3xl">{{ $product->name }}</h3>
                    <p class="text-gray-600 text-sm mt-1">{{ $product->description }}</p>
                </div>
                <img class="h-56 w-full object-cover mt-2" src="{{ Storage::url($product->preview) }}" alt="{{ $product->name }}">
                <div class="flex items-center justify-between px-4 py-2 bg-gray-900">
                    <span class="text-gray-200 font-bold text-xl">{{ $product->price }} рублей</span>
                    <form action="{{ route('cart-add', $product) }}" method="post" class="m-2">
                        @csrf
                        <button type="submit" class="mb-2 px-3 py-1 bg-gray-200 text-sm text-gray-900 font-semibold rounded">Купить</button>
                        <button type="button" class="px-3 py-1 bg-gray-200 text-sm text-gray-900 font-semibold rounded"><a href="{{ route('product',[$category->alias,$product->id])}}">Подробнее</a></button>
                    </form>
                </div>
            </article>
        @endforeach
    </section>
@endsection
