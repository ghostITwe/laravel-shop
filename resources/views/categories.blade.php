@extends('layouts.master')
@section('title','Категории')
@section('content')
    <section class="grid grid-cols-3 m-auto sm:w-full md:w-full lg:w-4/5 xl:w-4/5">
        @foreach($categories as $category)
            <div class="sm:w-full md:w-full lg:w-2/5 xl:w-2/5 m-3 rounded shadow-lg overflow-hidden">
                <img class="h-30 w-full object-cover mt-2" src="{{ Storage::url($category->photo) }}" alt="{{ $category->name }}">
                <label class="uploader px-6 pb-4 flex items-center text-lg">
                    <button type="button" class="text-white mt-6 text-sm py-2.5 px-5 rounded-md bg-blue-500 hover:shadow-lg"><a href="{{ route('category',$category->alias) }}">{{ $category->name }}</a></button>
                </label>
            </div>
        @endforeach
    </section>
@endsection
