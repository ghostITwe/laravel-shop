@extends('layouts.master')
@section('title',$product->name)
@section('content')
    <article>
        <h1>{{ $product->name }}</h1>
        @foreach($images as $image)
            <img src="{{ Storage::url($image->photo) }}" alt="">
        @endforeach
    {!! $product->description !!}
    </article>
@endsection
