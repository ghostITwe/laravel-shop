<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link href="https://unpkg.com/tailwindcss@%5E2/dist/tailwind.min.css" rel="stylesheet">
    <title>Mobileon - @yield('title')</title>
</head>
<body>
<header class="jc-sb ai-c border-solid border-b-4 md:flex">
    <article class="flex"><a href="{{ route('main') }}"><img src="{{ Storage::url('mobileon.png') }}" alt="" class="logo"></a></article>
    <nav class="nav flex gap-50">
        <div class="gap-30 flex">
            <a href="{{ route('main') }}">Главная</a>
            <a href="{{ route('categories') }}">Категории</a>
            <a href="#">Предложения</a>
            <a href="#">Контакты</a>
        </div>
        <div class="gap-30 flex">
            @guest
                <a href="{{ route('sign') }}"><img src="{{ Storage::url('user.png') }}"  alt="user"></a>
            @else
                <div id="dropdown-menu" class="dropdown-menu">
                    <button onclick="DropProfileMenu()" type="button" class="user-btn focus:outline-none" id="btn-dropMenu">
                        <img src="{{ Storage::url(Auth::user()->photo) }}" alt="{{ Auth::user()->name }}" class="h-avatar">
                    </button>
                    <ul>
                        @if(Auth::user()->role == 'Администратор')
                            <li><a href="{{ route('adminPanel') }}">Панель Администратора</a></li>
                        @endif
                        <li><a href="{{ route('profile.profile') }}">Профиль</a></li>
                        <li><a href="{{ route('profile.edit') }}">Настройки</a></li>
                        <li><a href="{{ route('logout') }}" onclick="UserLogout()">Выйти</a></li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </ul>
                </div>
            @endguest
            <a href="{{ route('cart') }}"><img src="{{ Storage::url('cart.png') }}"  alt="cart"></a>
            <a href="#"><img src="{{ Storage::url('search.png') }}" alt="search"></a>
        </div>
    </nav>
</header>
<main id="app">
    @yield('content')
</main>
<footer>
</footer>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/script.js') }}"></script>
</body>
</html>
