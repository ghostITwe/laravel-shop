@extends('layouts.master')
@section('title','Редактировать категорию'.$category->id)
@section('content')
    <section class="flex flex-col items-center mt-6 gap-2">
        <form action="{{ route('Category.update', $category) }}" method="post" enctype="multipart/form-data" class="flex flex-col gap-1">
            @csrf
            @method('PATCH')
            <label for="">Название категории:</label>
            <input type="text" name="category-name" value="{{ $category->name }}" class="border border-gray-300 p-2 my-2 rounded-md focus:outline-none focus:ring-2 ring-blue-200">
            <label for="">Фотография</label>
            <img src="{{ Storage::url($category->photo) }}" alt="" >
            <input type="file" name="photo">
            <label for="">Алиас</label>
            <input type="text" name="alias" value="{{ $category->alias }}" class="border border-gray-300 p-2 my-2 rounded-md focus:outline-none focus:ring-2 ring-blue-200">
            <button type="submit" class="text-white mt-3 text-sm py-2.5 px-5 rounded-md bg-blue-500 hover:shadow-lg">Изменить категорию</button>
        </form>
        <a href="{{ route('Category.index') }}" class="text-center hover:text-blue-500">Вернуться обратно</a>
    </section>
@endsection
