@extends('layouts.master')
@section('title','Просмотр категории'.$category->id)
@section('content')
    <section class="w-4/5 mx-auto">
        <div class="flex justify-center flex-col sm:flex-row">
            <div class="sm:w-1/4 p-2">
                <div class="bg-white px-6 py-8 rounded-lg shadow-lg text-center">
                    <div class="mb-3">
                        <img class="w-auto mx-auto" src="{{ Storage::url($category->photo) }}" alt="{{ $category->name }}">
                    </div>
                    <h2 class="text-xl font-medium text-gray-700">{{ $category->name }}</h2>
                    <span class="text-gray-500 block mb-5">{{ $category->alias }}</span>
                </div>
                <a href="{{ route('Category.index') }}" class="text-center hover:text-blue-500">Вернуться обратно</a>
            </div>
        </div>
    </section>
@endsection
