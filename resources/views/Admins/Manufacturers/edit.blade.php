@extends('layouts.master')
@section('title','Редактировать производителя'.$manufacturer->id)
@section('content')
    <section class="flex flex-col items-center mt-6 gap-2">
        <form action="{{ route('Manufacturers.update', $manufacturer) }}" method="post" class="flex flex-col gap-1">
            @csrf
            @method('PATCH')
            <label for="">Производитель:</label>
            <input type="text" name="name" value="{{ $manufacturer->name }}" class="border border-gray-300 p-2 my-2 rounded-md focus:outline-none focus:ring-2 ring-blue-200">
            <button type="submit" class="text-white mt-3 text-sm py-2.5 px-5 rounded-md bg-blue-500 hover:shadow-lg">Изменить</button>
            <a href="{{ route('Manufacturers.index') }}" class="text-center hover:text-blue-500">Вернуться обратно</a>
        </form>
    </section>
@endsection
