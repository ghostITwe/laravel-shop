@extends('layouts.master')
@section('title','Все производители')
@section('content')
    <section class="overflow-x-auto">
        <div class="flex flex-col items-center">
            <a class="mt-6 hover:text-blue-500" href="{{ route('adminPanel') }}">Вернуться в админ панель</a>
            <button type="button" class="text-white mt-6 text-sm py-2.5 px-5 rounded-md bg-green-500 hover:bg-green-600 hover:shadow-lg">
                <a href="{{ route('Manufacturers.create') }}">Добавить производителя</a></button>
            @if(session()->get('success'))
                <div class="py-3 px-2 my-2 bg-yellow-300 text-yellow-800 rounded border border-yellow-600" role="alert">
                    {{ session()->get('success') }}
                </div>
            @endif
        </div>
        <div class="min-w-screen min-h-screen flex justify-center font-sans overflow-hidden">
            <div class="w-full lg:w-5/6">
                <div class="bg-white shadow-md rounded my-6">
                    <table class="min-w-max w-full table-auto">
                        <thead>
                        <tr class="bg-gray-200 text-gray-600 uppercase text-sm leading-normal">
                            <th class="py-3 px-6 text-center">№</th>
                            <th class="py-3 px-6 text-center">Наименование организации</th>
                            <th class="py-3 px-6 text-center">Действия</th>
                        </tr>
                        </thead>
                        <tbody class="text-gray-600 text-sm font-light">
                        @foreach($manufacturers as $manufacturer)
                            <tr class="border-b border-gray-200 hover:bg-gray-100">
                                <td class="py-3 px-6 text-center">
                                    <span>{{ $manufacturer->id }}</span>
                                </td>
                                <td class="py-3 px-6 text-center">
                                    <span>{{ $manufacturer->name }}</span>
                                </td>
                                <td class="py-3 px-6 text-center">
                                    <div class="flex item-center justify-center">
                                        <a href="{{ route('Manufacturers.edit',$manufacturer) }}">
                                            <div class="w-6 mr-2 transform hover:text-blue-500 hover:scale-110">
                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z" />
                                                </svg>
                                            </div>
                                        </a>
                                        <form action="{{ route('Manufacturers.destroy', $manufacturer) }}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button>
                                                <div class="w-6 mr-2 transform hover:text-blue-500 hover:scale-110">
                                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                                    </svg>
                                                </div>
                                            </button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection
