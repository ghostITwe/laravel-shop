@extends('layouts.master')
@section('title','Добавить вид оплаты')
@section('content')
    <section class="flex flex-col items-center mt-6 gap-2">
        <form action="{{ route('Deliveries.store') }}" method="post" class="flex flex-col gap-1">
            @csrf
            <label for="">Вид оплаты:</label>
            <input type="text" name="name" value="{{ old('name') }}" class="border border-gray-300 p-2 my-2 rounded-md focus:outline-none focus:ring-2 ring-blue-200">
            <label for="">Стоимость:</label>
            <input type="text" name="cost" value="{{ old('cost') }}" class="border border-gray-300 p-2 my-2 rounded-md focus:outline-none focus:ring-2 ring-blue-200">
            <button type="submit" class="text-white mt-3 text-sm py-2.5 px-5 rounded-md bg-blue-500 hover:shadow-lg">Создать</button>
        </form>
        <a href="{{ route('Deliveries.index') }}" class="text-center hover:text-blue-500">Вернуться обратно</a>
    </section>
@endsection
