@extends('layouts.master')
@section('title','Все пользователи')
@section('content')
    <section class="overflow-x-auto">
        <div class="flex flex-col items-center">
            <a class="mt-6 hover:text-blue-500" href="{{ route('adminPanel') }}">Вернуться в админ панель</a>
            @if(session()->get('success'))
                <div>
                    {{ session()->get('success') }}
                </div>
            @endif
        </div>
        <div class="min-w-screen min-h-screen flex justify-center font-sans overflow-hidden">
            <div class="w-full lg:w-5/6">
                <div class="bg-white shadow-md rounded my-6">
                    <table class="min-w-max w-full table-auto">
                        <thead>
                        <tr class="bg-gray-200 text-gray-600 uppercase text-sm leading-normal">
                            <th class="py-3 px-6 text-center">№</th>
                            <th class="py-3 px-6 text-center">Фамилия</th>
                            <th class="py-3 px-6 text-center">Имя</th>
                            <th class="py-3 px-6 text-center">Номер телефона</th>
                            <th class="py-3 px-6 text-center">Почта</th>
                            <th class="py-3 px-6 text-center">Роль</th>
                        </tr>
                        </thead>
                        <tbody class="text-gray-600 text-sm font-light">
                        @foreach($users as $user)
                            <tr class="border-b border-gray-200 hover:bg-gray-100">
                                <td class="py-3 px-6 text-center">
                                    <span>{{ $user->id }}</span>
                                </td>
                                <td class="py-3 px-6 text-center">
                                    <span>{{ $user->surname }}</span>
                                </td>
                                <td class="py-3 px-6 text-center">
                                    <span>{{ $user->name }}</span>
                                </td>
                                <td class="py-3 px-6 text-center">
                                    <span>{{ $user->phone }}</span>
                                </td>
                                <td class="py-3 px-6 text-center">
                                    <span>{{ $user->email }}</span>
                                </td>
                                <td class="py-3 px-6 text-center">
                                    <span>{{ $user->role }}</span>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection
