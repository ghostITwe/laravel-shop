@extends('layouts.master')
@section('title','Админ панель')
@section('content')
    <section class="admin-panel text-c">
        <h3>Добро пожаловать, вы зашли в панель администратора</h3>
        <h4>Выберите нужную таблицу, для работы с ней:</h4>
        <ul>
            <li><a href="{{ route('Category.index')}}">Категории</a></li>
            <li><a href="{{ route('Deliveries.index') }}">Способы доставки</a></li>
            <li><a href="{{ route('Payments.index') }}">Способы оплаты</a></li>
            <li><a href="{{ route('Manufacturers.index') }}">Производители</a></li>
            <li><a href="{{ route('Products.index') }}">Товары</a></li>
            <li><a href="{{ route('Orders.index') }}">Заказы</a></li>
            <li><a href="{{ route('Users.index') }}">Пользователи</a></li>
        </ul>
    </section>
@endsection
