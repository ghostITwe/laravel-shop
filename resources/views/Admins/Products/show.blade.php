@extends('layouts.master')
@section('title','Просмотр товара'.$product->id)
@section('content')
    <section>
        <h3>{{ $product->name }}</h3>
        <span>{{ $product->price }}</span>
        <span>{{ $product->count }}</span>
        <span>{{ $product->manufacturer->name }}</span>
        <span> {{ App\Models\Catproduct::find($product->catproduct_id)->value('name') }}</span>
        @foreach($images as $image)
            <img src="{{ Storage::url($image->photo) }}" alt="">
        @endforeach
        <textarea name="description" cols="30" rows="10">{!! $product->description !!}</textarea>
        <a href="{{ route('Products.index') }}">Вернуться обратно</a>
    </section>
@endsection
