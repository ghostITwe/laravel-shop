@extends('layouts.master')
@section('title','Редактировать товар'.$product->id)
@section('content')
    <form action="{{ route('Products.update', $product) }}" method="post" enctype="multipart/form-data">
        @csrf
        @method('PATCH')
        <label for="">Название товара:</label>
        <input type="text" name="name" value="{{ $product->name }}">
        <label for="">Цена</label>
        <input type="text" name="price" value="{{ $product->price }}">
        <label for="">Количество</label>
        <input type="text" name="count" value="{{ $product->count }}">
        <label for="">Производитель</label>
        <select name="manufacturer">
            @foreach($manufacturers as $manufacturer)
                <option value="{{ $manufacturer->id }}">{{ $manufacturer->name }}</option>
            @endforeach
        </select>
        <label for="">Категория</label>
        <select name="category">
            @foreach($categories as $category)
                <option value="{{ $category->id }}">{{ $category->name }}</option>
            @endforeach
        </select>
        <label for="">Фотография</label>
        <img src="{{ Storage::url($product->preview) }}" alt="">
        <input type="file" name="photo">
        <label for="">Описание</label>
        <textarea name="description" id="" cols="30" rows="10">{!! $product->description !!}</textarea>
        <button type="submit">Изменить товар</button>
    </form>
@endsection
