@extends('layouts.master')
@section('title','Добавить товар')
@section('content')
    <form action="{{ route('Products.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <label for="">Название товара</label>
        <input type="text" name="name">
        <label for="">Цена</label>
        <input type="text" name="price">
        <label for="">Количество</label>
        <input type="text" name="count">
        <label for="">Производитель</label>
        <select name="manufacturer">
            @foreach($manufacturers as $manufacturer)
                <option value="{{ $manufacturer->id }}">{{ $manufacturer->name }}</option>
            @endforeach
        </select>
        <label for="">Категория</label>
        <select name="category">
            @foreach($categories as $category)
                <option value="{{ $category->id }}">{{ $category->name }}</option>
            @endforeach
        </select>
        <label for="">Фотография</label>
        <input type="file" name="photo">
        <label for="">Описание товара</label>
        <textarea name="description" cols="30" rows="10"></textarea>
        <button type="submit">Создать</button>
    </form>
    <a href="{{ route('Products.index') }}">Вернуться обратно</a>
@endsection
