@extends('layouts.master')
@section('title','Редактирование заказа'.' '.$order->id)
@section('content')
    <section class="text-c flex fd-c">
        <span>Измените статус заказа под номером - {{ $order->id }}</span>
        <form action="{{ route('Orders.update', $order) }}" method="post">
            @csrf
            @method('PATCH')
            <label for="">Статус заказа</label>
            <input type="text" name="status" value="{{ $order->status }}">
            <button type="submit">Изменить</button>
        </form>
        <a href="{{ route('Orders.index') }}">Вернуться обратно</a>
    </section>
@endsection
