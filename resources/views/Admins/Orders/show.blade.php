@extends('layouts.master')
@section('title','Просмотр заказа'.' '.$order->id)
@section('content')
    <section class="overflow-x-auto">
        <div class="flex flex-col items-center">
            <a class="mt-6 mb-2 hover:text-blue-500" href="{{ route('Orders.index') }}">Вернуться обратно</a>
            <span>Покупатель - {{ $order->user->email }}</span>
        </div>
        <div class="min-w-screen min-h-screen flex justify-center font-sans overflow-hidden">
            <div class="w-full lg:w-5/6">
                <div class="bg-white shadow-md rounded my-6">
                    <table class="min-w-max w-full table-auto">
                        <thead>
                        <tr class="bg-gray-200 text-gray-600 uppercase text-sm leading-normal">
                            <th class="py-3 px-6 text-center">№</th>
                            <th class="py-3 px-6 text-center">Наименование товара</th>
                            <th class="py-3 px-6 text-center">Стоимость</th>
                            <th class="py-3 px-6 text-center">Количество</th>
                            <th class="py-3 px-6 text-center">Способ доставки</th>
                            <th class="py-3 px-6 text-center">Способ оплаты</th>
                        </tr>
                        </thead>
                        <tbody class="text-gray-600 text-sm font-light">
                        @foreach($order->products as $product)
                            <tr class="border-b border-gray-200 hover:bg-gray-100">
                                <td class="py-3 px-6 text-center">
                                    <span>{{ $product->pivot->tovar_id }}</span>
                                </td>
                                <td class="py-3 px-6 text-center">
                                    <span>{{ $product->name }}</span>
                                </td>
                                <td class="py-3 px-6 text-center">
                                    <span>{{ $product->price }}</span>
                                </td>
                                <td class="py-3 px-6 text-center">
                                    <span>{{ $product->pivot->count }}</span>
                                </td>
                                <td class="py-3 px-6 text-center">
                                    <span>{{ $order->delivery->name }}</span>
                                </td>
                                <td class="py-3 px-6 text-center">
                                    <span>{{ $order->payment->type }}</span>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection
