@extends('layouts.master')
@section('title','Личный кабинет')
@section('content')
<section class="flex justify-center">
    <div class="max-w-xs">
        <div class="bg-white shadow-xl rounded-lg py-3">
            <div class="photo-wrapper p-2">
                <img class="w-32 h-32 rounded-full mx-auto" src="{{ Storage::url(Auth::user()->photo) }}" alt="{{ Auth::user()->name }}">
            </div>
            <div class="p-2 items-center">
                <h3 class="text-center text-xl text-gray-900 font-medium leading-8">{{ Auth::user()->name.' '.Auth::user()->surname }}</h3>
                <table class="text-xs my-3">
                    <tbody>
                    <tr>
                        <td class="px-2 py-2 text-gray-500 font-semibold">Номер телефона</td>
                        <td class="px-2 py-2">{{ Auth::user()->phone }}</td>
                    </tr>
                    <tr>
                        <td class="px-2 py-2 text-gray-500 font-semibold">Почта</td>
                        <td class="px-2 py-2">{{ Auth::user()->email }}</td>
                    </tr>
                    </tbody>
                </table>
                <button type="button" class="text-white mt-6 text-sm py-2.5 px-5 rounded-md bg-blue-500 hover:bg-green-600 hover:shadow-lg">
                    <a href="{{ route('profile.edit') }}">Изменить профиль</a></button>
            </div>
        </div>
    </div>
</section>
@endsection
