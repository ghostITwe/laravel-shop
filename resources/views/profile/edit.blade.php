@extends('layouts.master')
@section('title','Личный кабинет')
@section('content')
    <section class="flex flex-col items-center mt-6 gap-2">
        <form method="POST" action="{{ route('profile.edit') }}" enctype="multipart/form-data" class="flex flex-col gap-1">
            @csrf
            <label for="surname">Фамилия</label>
            <input id="surname" type="text" name="surname"
                   value="{{ Request::old('surname') ?: Auth::user()->surname }}" class="border border-gray-300 p-2 my-2 rounded-md focus:outline-none focus:ring-2 ring-blue-200">
            <label for="name">Имя</label>
            <input id="name" type="text" name="name"
                   value="{{ Request::old('name') ?: Auth::user()->name }}" class="border border-gray-300 p-2 my-2 rounded-md focus:outline-none focus:ring-2 ring-blue-200">
            <label for="phone">Номер телефона</label>
            <input id="phone" type="tel" name="phone"
                   value="{{ Request::old('phone') ?: Auth::user()->phone }}" class="border border-gray-300 p-2 my-2 rounded-md focus:outline-none focus:ring-2 ring-blue-200">
            <label for="email">Электронная почта</label>
            <input id="email" type="email" name="email"
                   value="{{ Request::old('email') ?: Auth::user()->email }}" class="border border-gray-300 p-2 my-2 rounded-md focus:outline-none focus:ring-2 ring-blue-200">
            <label for="login">Логин</label>
            <input id="login" type="text" name="login"
                   value="{{ Request::old('login') ?: Auth::user()->login }}" class="border border-gray-300 p-2 my-2 rounded-md focus:outline-none focus:ring-2 ring-blue-200">
            <label for="photo">Аватар</label>
            <input type="file" name="photo">
            <button type="submit" class="text-white mt-3 text-sm py-2.5 px-5 rounded-md bg-blue-500 hover:shadow-lg">Сохранить изменения</button>
            <a href="{{ route('profile.profile') }}" class="text-center hover:text-blue-500">Вернуться обратно</a>
        </form>
    </section>
@endsection
