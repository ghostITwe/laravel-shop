<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTovarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tovars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->decimal('price');
            $table->decimal('new_price')->nullable();
            $table->integer('count');
            $table->unsignedBigInteger('manufacturer')->unsigned();
            $table->foreign('manufacturer')->references('id')->on('manufacturers');
            $table->unsignedBigInteger('catproduct')->unsigned();
            $table->foreign('catproduct')->references('id')->on('catproducts');
            $table->text('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tovars');
    }
}
