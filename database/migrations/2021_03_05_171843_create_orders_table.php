<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user')->unsigned();
            $table->foreign('user')->references('id')->on('users');
            $table->decimal('amount');
            $table->dateTime('order_date');
            $table->unsignedBigInteger('delivery')->unsigned();
            $table->unsignedBigInteger('payment')->unsigned();
            $table->foreign('delivery')->references('id')->on('deliveries');
            $table->foreign('payment')->references('id')->on('payments');
            $table->string('status');
            $table->string('comment');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
