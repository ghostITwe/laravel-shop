<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ViewUsersInformation extends Model
{
    public $table = "users_information";
}
