<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    public function tovar()
    {
        return $this->belongsTo(Tovar::class);
    }
}
