<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    protected $fillable = [
        'name',
        'cost'
    ];

    public function orders() {
        return $this->belongsTo(Order::class);
    }
}
