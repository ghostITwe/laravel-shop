<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'amount',
        'order_date',
        'delivery_id',
        'payment_id',
        'comment'
    ];

    public function products() {
        return $this->belongsToMany(Tovar::class)->withPivot('count')->withTimestamps();
    }

    public function delivery() {
        return $this->belongsTo(Delivery::class);
    }

    public function payment() {
        return $this->belongsTo(Payment::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function amount() {
        $sum = 0;
        foreach ($this->products as $product) {
            $sum += $product->getPriceForCount();
        }
        $deliver = new Delivery();
        if ($deliver->name = "Курьером") {
            $sum += 1000;
            return $sum;
        } else {
            return $sum;
        }
    }
}
