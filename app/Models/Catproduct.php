<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Catproduct extends Model
{
    protected  $table = 'catproducts';

    protected $fillable = [
      'name','photo','alias'
    ];

    public function products() {
        return $this->hasMany(Tovar::class);
    }
}
