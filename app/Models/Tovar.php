<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tovar extends Model
{
    protected $table = 'tovars';

    public function Images() {
        return $this->hasMany(Image::class);
    }

    public function manufacturer() {
        return $this->belongsTo(Manufacturer::class);
    }

    public function category() {
        return $this->belongsTo(Catproduct::class);
    }

    public function getPriceForCount() {
        if (!is_null($this->pivot)) {
            if ($this->discount = 1) {
                $discountprice = $this->price * 0.9;
                return $this->pivot->count * $discountprice;
            } else {
                return $this->pivot->count * $this->price;
            }
        }
        return $this->price;
    }
}
