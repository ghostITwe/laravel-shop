<?php

namespace App\Http\Middleware;

use App\Models\Order;
use Closure;

class CartIsNotEmpty
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $orderId = session('orderId');
        if (!is_null($orderId)) {
            $order = Order::findOrFail($orderId);
            if ($order->products->count() == 0) {
                $delorder = Order::find($order->id);
                $delorder->delete();
                session()->forget('orderId');
                return redirect()->route('cart');
            }
        }
        return $next($request);
    }
}
