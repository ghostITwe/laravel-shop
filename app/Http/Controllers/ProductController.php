<?php

namespace App\Http\Controllers;

use App\Models\Catproduct;
use App\Models\Image;
use App\Models\Tovar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function showproduct($category_alias,$id = null)
    {
        $product = Tovar::where('id',$id)->first();
        $images = Tovar::find($id)->Images;

        return view('product',compact('images','product'));
    }

    public function categories() {
        return view('categories');
    }

    public function category($category_alias) {
        $category = Catproduct::where('alias',$category_alias)->first();
        $count = DB::selectOne("Select ProductCountCategory(?) as count",[$category->id]);
        return view('category',compact('category', 'count'));
    }


}
