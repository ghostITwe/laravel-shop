<?php

namespace App\Http\Controllers;

use App\Models\Catproduct;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index() {
        return view('index');
    }

    public function signinup() {
        return view('auth.signinup');
    }
}
