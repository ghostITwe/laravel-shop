<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class   ProfileController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        return view ('profile.profile');
    }

    public function getEdit()
    {
        return view('profile.edit');
    }

    public function postEdit(Request $request)
    {
        $this->validate($request, [
            'surname' => 'alpha|max:100',
            'name' => 'alpha|max:100',
        ]);

        if (empty($request->file('photo'))) {
            $avatar = Auth::user()->photo;
        } else {
            $avatar = $request->file('photo')->store('user');
        }

        Auth::user()->update([
            'surname' => $request->input('surname'),
            'name' => $request->input('name'),
            'phone' => $request->input('phone'),
            'email' => $request->input('email'),
            'login' => $request->input('login'),
            'photo' => $avatar
        ]);

        return redirect()->route('profile.profile');
    }
}
