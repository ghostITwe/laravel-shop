<?php

namespace App\Http\Controllers;

use App\Models\Delivery;
use App\Models\Order;
use App\Models\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Monolog\Handler\IFTTTHandler;

class CartController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function cart() {
        $orderId = session('orderId');
        if (!is_null($orderId)) {
            $order = Order::findOrFail($orderId);
        } else {
            $order = null;
        }
        $shipping = Delivery::all();
        $payments = Payment::all();
        return view('cart', compact('order','shipping', 'payments'));
    }

    public function cartСonfirm() {
        $orderId = session('orderId');
        if (!is_null($orderId)) {
           $order = Order::find($orderId);
           $deliver = $_POST["deliver"];
           $pay = $_POST["pay"];
           $comm = $_POST["comment"];
           Order::find($orderId)->update([
              'amount' => $order->amount(),
              'order_date' => date("Y-m-d H:i:s"),
               'delivery_id' => $deliver,
               'payment_id' => $pay,
               'comment' => $comm
           ]);
           session()->forget('orderId');
        }
        return redirect()->route('main');
    }

    public function cartAdd($productId) {
        $orderId = session('orderId');
        if (is_null($orderId)) {
            $order = Order::create([
                'user_id' => Auth::user()->id
            ]);
            session(['orderId' => $order->id]);
        } else {
            $order = Order::find($orderId);
        }

        if ($order->products->contains($productId)) {
            $pivotRow = $order->products()->where('tovar_id', $productId)->first()->pivot;
            $pivotRow->count++;
            $pivotRow->update();
        } else {
            $order->products()->attach($productId);
        }
        return redirect()->route('cart');
    }

    public function cartRemove($productId) {
        $orderId = session('orderId');
        if (is_null($orderId)) {
            return redirect()->route('cart');
        }
        $order = Order::find($orderId);

        if ($order->products->contains($productId)) {
            $pivotRow = $order->products()->where('tovar_id', $productId)->first()->pivot;
            if ($pivotRow->count < 2) {
                $order->products()->detach($productId);
            } else {
                $pivotRow->count--;
                $pivotRow->update();
            }
        }
        return redirect()->route('cart');
    }
}
