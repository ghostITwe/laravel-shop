<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Payment;
use Illuminate\Http\Request;

class PaymentsController extends Controller
{
    public function index() {
        $payments = Payment::all();

        return view('Admins.Payments.index', compact('payments'));
    }

    public function create() {
        return view('Admins.Payments.create');
    }

    public function store(Request $request) {
        $request->validate([
            'type' => 'required'
        ]);

        $payment = new Payment([
            'type' => $request->get('type')
        ]);
        $payment->save();

        return redirect('/adminPanel/Payments')->with('success','Вид оплаты был добавлен');
    }

    public function show($id) {

        $payment = Payment::find($id);
        return view('Admins.Payments.show', compact('payment'));
    }

    public function edit($id) {
        $payment = Payment::find($id);

        return view('Admins.Payments.edit',compact('payment'));
    }

    public function update(Request $request, $id) {
        $request->validate([
            'type' => 'required'
        ]);

        $payment = Payment::find($id);
        $payment->type = $request->get('type');
        $payment->save();

        return redirect('/adminPanel/Payments')->with('success','Вид оплаты был изменен');
    }

    public function destroy($id) {
        $payment = Payment::find($id);
        $payment->delete();

        return redirect('/adminPanel/Payments')->with('success','Вид оплаты был удален');
    }
}
