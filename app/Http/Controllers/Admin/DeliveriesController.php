<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Delivery;
use Illuminate\Http\Request;

class DeliveriesController extends Controller
{
    public function index() {

        $deliveries = Delivery::all();

        return view('Admins.Deliveries.index', compact('deliveries'));
    }

    public function create() {
        return view('Admins.Deliveries.create');
    }

    public function store(Request $request) {
        $request->validate([
            'name' => 'required',
            'cost' => 'required'
        ]);

        $deliveries = new Delivery([
            'name' => $request->get('name'),
            'cost' => $request->get('cost')
        ]);
        $deliveries->save();

        return redirect('/adminPanel/Deliveries')->with('success','Производитель был добавлен');
    }

    public function show($id) {

        $deliveries = Delivery::find($id);
        return view('Admins.Deliveries.show', compact('deliveries'));
    }

    public function edit($id) {
        $deliveries = Delivery::find($id);

        return view('Admins.Deliveries.edit',compact('deliveries'));
    }

    public function update(Request $request, $id) {
        $request->validate([
            'name' => 'required',
            'cost' => 'required'
        ]);

        $deliveries = Delivery::find($id);
        $deliveries->name = $request->get('name');
        $deliveries->cost = $request->get('cost');
        $deliveries->save();

        return redirect('/adminPanel/Deliveries')->with('success','Способ достави был изменен');
    }

    public function destroy($id) {
        $manufacturer = Manufacturer::find($id);
        $manufacturer->delete();

        return redirect('/adminPanel/Deliveries')->with('success','Cпособ доставки был удален');
    }
}
