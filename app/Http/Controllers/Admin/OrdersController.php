<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use http\Client\Curl\User;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    public function index() {
        $orders = Order::all();

        return view('Admins.Orders.index', compact('orders'));
    }

    public function show($id) {
        $order = Order::find($id);

        return view('Admins.Orders.show', compact('order'));
    }

    public function edit($id) {
        $order = Order::find($id);

        return view('Admins.Orders.edit',compact('order'));
    }

    public function update(Request $request, $id) {
        $request->validate([
           'status' => 'required'
        ]);

        $order = Order::find($id);
        $order->status = $request->get('status');
        $order->save();

        return redirect('/adminPanel/Orders')->with('success','Статус заказа был успешно изменён');
    }
}
