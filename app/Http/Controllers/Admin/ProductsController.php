<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Catproduct;
use App\Models\Manufacturer;
use App\Models\Payment;
use App\Models\Tovar;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function index() {
        $products = Tovar::all();
        return view('Admins.Products.index', compact('products'));
    }

    public function create() {

        $categories = Catproduct::all();
        $manufacturers = Manufacturer::all();

        return view('Admins.Products.create', compact('categories', 'manufacturers'));
    }

        public function store(Request $request) {
        $request->validate([
            'name' => 'required',
            'price' => 'required',
            'count' => 'required',
            'manufacturer' => 'required',
            'category' => 'required',
            'description' => 'string'
//            'photo' => 'required',
        ]);

        $product = new Tovar([
            'name' => $request->get('name'),
            'price' => $request->get('price'),
            'count' => $request->get('count'),
            'manufacturer_id' => $_POST["manufacturer"],
            'catproduct_id' => $_POST["category"],
            'description' => $_POST['description']
        ]);
        $product->save();

        return redirect('/adminPanel/Products')->with('success','Товар был добавлен');
    }

    public function show($id) {

        $product = Tovar::find($id);
        $images = Tovar::find($id)->Images;

        return view('Admins.Products.show', compact('product','images'));
    }

    public function edit($id) {
        $product = Tovar::find($id);
        $categories = Catproduct::all();
        $manufacturers = Manufacturer::all();

        return view('Admins.Products.edit',compact('product','categories','manufacturers'));
    }

    public function update(Request $request, $id) {
        $request->validate([
            'name' => '',
            'price' => '',
            'count' => '',
            'manufacturer' => '',
            'category' => '',
            'description' => 'string'
        ]);

        $product = Tovar::find($id);
        $product->name = $request->get('name');
        $product->price = $request->get('price');
        $product->count = $request->get('count');
        $product->manufacturer_id = $_POST["manufacturer"];
        $product->catproduct_id = $_POST["category"];
        $product->description = $_POST['description'];
        $product->save();

        return redirect('/adminPanel/Products')->with('success','Товар был изменен');
    }

    public function destroy($id) {
        $product = Tovar::find($id);
        $product->delete();

        return redirect('/adminPanel/Products')->with('success','Товар был удален');
    }
}
