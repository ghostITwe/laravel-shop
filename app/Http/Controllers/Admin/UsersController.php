<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ViewUsersInformation;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function index() {
        $users = ViewUsersInformation::all();

        return view('Admins.Users.index', compact('users'));
    }
}
