<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Manufacturer;
use Illuminate\Http\Request;

class ManufacturersController extends Controller
{
    public function index() {
        $manufacturers = Manufacturer::all();

        return view('Admins.Manufacturers.index', compact('manufacturers'));
    }

    public function create() {
        return view('Admins.Manufacturers.create');
    }

    public function store(Request $request) {
        $request->validate([
            'name' => 'required'
        ]);

        $manufacturer = new Manufacturer([
            'name' => $request->get('name')
        ]);
        $manufacturer->save();

        return redirect('/adminPanel/Manufacturers')->with('success','Производитель был добавлен');
    }

    public function show($id) {

        $manufacturer = Manufacturer::find($id);
        return view('Admins.Manufacturers.show', compact('manufacturer'));
    }

    public function edit($id) {
        $manufacturer = Manufacturer::find($id);

        return view('Admins.Manufacturers.edit',compact('manufacturer'));
    }

    public function update(Request $request, $id) {
        $request->validate([
            'name' => 'required'
        ]);

        $manufacturer = Manufacturer::find($id);
        $manufacturer->name = $request->get('name');
        $manufacturer->save();

        return redirect('/adminPanel/Manufacturers')->with('success','Производитель был изменен');
    }

    public function destroy($id) {
        $manufacturer = Manufacturer::find($id);
        $manufacturer->delete();

        return redirect('/adminPanel/Manufacturers')->with('success','Производитель был удалён');
    }
}
