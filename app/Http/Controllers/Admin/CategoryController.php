<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Catproduct;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index() {
        $categories = Catproduct::all();

        return view('Admins.Categories.index', compact('categories'));
    }

    public function create() {
        return view('Admins.Categories.create');
    }

    public function store(Request $request) {
        $request->validate([
            'category-name' => 'required',
            'photo' => 'required',
            'alias' => 'required'
        ]);

        $category = new Catproduct([
            'name' => $request->get('category-name'),
            'photo' => $request->file('photo')->store('category'),
            'alias' => $request->get('alias')
        ]);
        $category->save();

        return redirect('/adminPanel/Category')->with('success','Категория была добавлена');
    }

    public function show($id) {

        $category = Catproduct::find($id);
        return view('Admins.Categories.show', compact('category'));
    }

    public function edit($id) {
        $category = Catproduct::find($id);

        return view('Admins.Categories.edit',compact('category'));
    }

    public function update(Request $request, $id) {
        $request->validate([
            'category-name' => 'required',
            'alias' => 'required'
        ]);

        $category = Catproduct::find($id);
        $category->name = $request->get('category-name');
        $category->photo = $request->file('photo')->store('category');
        $category->alias = $request->get('alias');
        $category->save();

        return redirect('/adminPanel/Category')->with('success','Категория была изменена');
    }


    public function destroy($id) {
        $category = Catproduct::find($id);
        $category->delete();

        return redirect('/adminPanel/Category')->with('success','Категория была удалена');
    }
}
